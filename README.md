# iceberg-lightline

This repository takes the [lightline](https://github.com/itchyny/lightline.vim)
plugin from [iceberg.vim](https://github.com/cocopon/iceberg.vim) and puts it
into a single repository for use alongside [vim-colorschemes]().
